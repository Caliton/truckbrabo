import 'package:sqfentity/sqfentity.dart';

class GasStation extends SqfEntityTable {
  GasStation() {
    tableName = 'gasStation';
    modelName = 'GasStation';
    primaryKeyName = 'id';
    primaryKeyisIdentity = true;
    
    fields = [
      SqfEntityField('name', DbType.text),
      SqfEntityField('city', DbType.text),
    ];
    super.init();
  }

  static SqfEntityTable _instance;
  static SqfEntityTable get getInstance {
    if (_instance == null) {
      _instance = GasStation();
    }
    return _instance;
  }
}