import 'package:sqfentity/sqfentity.dart';
import 'package:truckbrabo/models/gas_station.dart';
import 'package:truckbrabo/models/truck.dart';

class Supply extends SqfEntityTable {
  Supply() {
    tableName = 'supply';
    modelName = 'Supply';
    primaryKeyName = 'id';
    primaryKeyisIdentity = true;
    
    fields = [
      SqfEntityField('name', DbType.text),
      SqfEntityField('date', DbType.integer),
      SqfEntityField('kmOld', DbType.real),
      SqfEntityField('kmNow', DbType.real),
      SqfEntityField('qtyLiters', DbType.real),
      SqfEntityField('avg', DbType.real),
      SqfEntityField('expense', DbType.real),
      SqfEntityField('gainGross', DbType.real),
      SqfEntityField('gainNet', DbType.real),
      SqfEntityField('gainKm', DbType.real),
      SqfEntityFieldRelationship(Truck.getInstance, DeleteRule.CASCADE, defaultValue: "0"),
      SqfEntityFieldRelationship(GasStation.getInstance, DeleteRule.CASCADE, defaultValue: "0"),
    ];
    super.init();
  }
  static SqfEntityTable _instance;
  static SqfEntityTable get getInstance {
    if (_instance == null) {
      _instance = Supply();
    }
    return _instance;
  }
}