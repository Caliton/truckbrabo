import 'package:sqfentity/sqfentity.dart';

class Truck extends SqfEntityTable {
  Truck() {
    tableName = 'truck';
    modelName =  'Truck';
    primaryKeyName = 'id';
    primaryKeyisIdentity = true;
    
    fields = [
      SqfEntityField('truck', DbType.text),
      SqfEntityField('driver', DbType.text),
    ];
    super.init();
  }

  static SqfEntityTable _instance;
  static SqfEntityTable get getInstance {
    if (_instance == null) {
      _instance = Truck();
    }
    return _instance;
  }
}