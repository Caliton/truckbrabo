import 'package:sqfentity/sqfentity.dart';
import 'package:truckbrabo/models/gas_station.dart';
import 'package:truckbrabo/models/truck.dart';
import 'package:truckbrabo/models/supply.dart';

class MyDbModel extends SqfEntityModel {
  MyDbModel() {
    databaseName = "dbtruck.db";
    databaseTables = [
      GasStation.getInstance,
      Truck.getInstance,
      Supply.getInstance,
    ];
    bundledDatabasePath = null;
  }
}