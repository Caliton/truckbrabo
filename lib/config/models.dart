import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:sqfentity/sqfentity.dart';
import 'package:truckbrabo/config/database_config.dart';

// region GasStation
class GasStation {
  GasStation({this.id, this.name, this.city}) {
    setDefaultValues();
  }
  GasStation.withFields(this.name, this.city) {
    setDefaultValues();
  }
  GasStation.withId(this.id, this.name, this.city) {
    setDefaultValues();
  }
  GasStation.fromMap(Map<String, dynamic> o) {
    id = o["id"] as int;
    name = o["name"] as String;
    city = o["city"] as String;
  }
  // FIELDS
  int id;
  String name;
  String city;
  // end FIELDS

// COLLECTIONS
  SupplyFilterBuilder getSupplies(
      {List<String> columnsToSelect, bool getIsDeleted}) {
    return Supply()
        .select(columnsToSelect: columnsToSelect, getIsDeleted: getIsDeleted)
        .gasStationId
        .equals(id);
  }
  // END COLLECTIONS

  static const bool _softDeleteActivated = false;
  GasStationManager __mnGasStation;
  GasStationFilterBuilder _select;

  GasStationManager get _mnGasStation {
    if (__mnGasStation == null) __mnGasStation = GasStationManager();
    return __mnGasStation;
  }

  // methods
  Map<String, dynamic> toMap({bool forQuery = false}) {
    final map = Map<String, dynamic>();
    if (id != null) {
      map["id"] = id;
    }
    if (name != null) {
      map["name"] = name;
    }
    if (city != null) {
      map["city"] = city;
    }

    return map;
  }

  List<dynamic> toArgs() {
    return [id, name, city];
  }

  static Future<List<GasStation>> fromWebUrl(String url,
      [VoidCallback gasstationList(List<GasStation> o)]) async {
    var objList = List<GasStation>();
    try {
      final response = await http.get(url);
      final Iterable list = json.decode(response.body) as Iterable;
      objList = list
          .map((gasstation) =>
              GasStation.fromMap(gasstation as Map<String, dynamic>))
          .toList();
      if (gasstationList != null) {
        gasstationList(objList);
      }
      return objList;
    } catch (e) {
      print("SQFENTITY ERROR GasStation.fromWeb: ErrorMessage:" + e.toString());
      return null;
    }
  }

  static Future<List<GasStation>> fromObjectList(
      Future<List<dynamic>> o) async {
    final gasstationsList = List<GasStation>();
    final data = await o;
    for (int i = 0; i < data.length; i++) {
      gasstationsList.add(GasStation.fromMap(data[i] as Map<String, dynamic>));
    }
    return gasstationsList;
  }

  static List<GasStation> fromMapList(List<Map<String, dynamic>> query) {
    final List<GasStation> gasstations = List<GasStation>();
    for (Map map in query) {
      gasstations.add(GasStation.fromMap(map as Map<String, dynamic>));
    }
    return gasstations;
  }

  /// returns GasStation by ID if exist, otherwise returns null
  /// <param name="id">Primary Key Value</param>
  /// <returns>returns GasStation if exist, otherwise returns null</returns>
  Future<GasStation> getById(int id) async {
    GasStation gasstationObj;
    final data = await _mnGasStation.getById(id);
    if (data.length != 0) {
      gasstationObj = GasStation.fromMap(data[0] as Map<String, dynamic>);
    } else {
      gasstationObj = null;
    }
    return gasstationObj;
  }

  /// <summary>
  /// Saves the object. If the id field is null, saves as a new record and returns new id, if id is not null then updates record
  /// </summary>
  /// <returns>Returns id</returns>
  Future<int> save() async {
    if (id == null || id == 0) {
      id = await _mnGasStation.insert(GasStation.withFields(name, city));
    } else {
      id = await _upsert();
    }
    return id;
  }

  /// <summary>
  /// saveAll method saves the sent List<GasStation> as a batch in one transaction
  /// </summary>
  /// <returns> Returns a <List<BoolResult>> </returns>
  Future<List<BoolResult>> saveAll(List<GasStation> gasstations) async {
    final results = _mnGasStation.saveAll(
        "INSERT OR REPLACE INTO gasStation (id, name,city)  VALUES (?,?,?)",
        gasstations);
    return results;
  }

  /// <summary>
  /// Updates if the record exists, otherwise adds a new row
  /// </summary>
  /// <returns>Returns id</returns>
  Future<int> _upsert() async {
    id = await _mnGasStation.rawInsert(
        "INSERT OR REPLACE INTO gasStation (id, name,city)  VALUES (?,?,?)",
        [id, name, city]);
    return id;
  }

  /// <summary>
  /// inserts or replaces the sent List<Todo> as a batch in one transaction.
  /// upsertAll() method is faster then saveAll() method. upsertAll() should be used when you are sure that the primary key is greater than zero
  /// </summary>
  /// <returns> Returns a <List<BoolResult>> </returns>
  Future<List<BoolResult>> upsertAll(List<GasStation> gasstations) async {
    final results = await _mnGasStation.rawInsertAll(
        "INSERT OR REPLACE INTO gasStation (id, name,city)  VALUES (?,?,?)",
        gasstations);
    return results;
  }

  /// <summary>
  /// saveAs GasStation. Returns a new Primary Key value of GasStation
  /// </summary>
  /// <returns>Returns a new Primary Key value of GasStation</returns>
  Future<int> saveAs() async {
    id = await _mnGasStation.insert(GasStation.withFields(name, city));
    return id;
  }

  /// <summary>
  /// Deletes GasStation
  /// </summary>
  /// <returns>BoolResult res.success=Deleted, not res.success=Can not deleted</returns>
  Future<BoolResult> delete([bool hardDelete = false]) async {
    print("SQFENTITIY: delete GasStation invoked (id=$id)");
    var result = BoolResult();
    {
      result =
          await Supply().select().gasStationId.equals(id).delete(hardDelete);
    }
    if (!result.success) {
      return result;
    } else if (!_softDeleteActivated || hardDelete) {
      return _mnGasStation.delete(QueryParams(whereString: "id=$id"));
    } else {
      return _mnGasStation
          .updateBatch(QueryParams(whereString: "id=$id"), {"isDeleted": 1});
    }
  }

  //private GasStationFilterBuilder _Select;
  GasStationFilterBuilder select(
      {List<String> columnsToSelect, bool getIsDeleted}) {
    _select = GasStationFilterBuilder(this);
    _select._getIsDeleted = getIsDeleted == true;
    _select.qparams.selectColumns = columnsToSelect;
    return _select;
  }

  GasStationFilterBuilder distinct(
      {List<String> columnsToSelect, bool getIsDeleted}) {
    final GasStationFilterBuilder _distinct = GasStationFilterBuilder(this);
    _distinct._getIsDeleted = getIsDeleted == true;
    _distinct.qparams.selectColumns = columnsToSelect;
    _distinct.qparams.distinct = true;
    return _distinct;
  }

  void setDefaultValues() {}
  //end methods
}
// endregion gasstation

// region GasStationField
class GasStationField extends SearchCriteria {
  GasStationField(this.gasstationFB) {
    param = DbParameter();
  }
  DbParameter param;
  String _waitingNot = "";
  GasStationFilterBuilder gasstationFB;

  GasStationField get not {
    _waitingNot = " NOT ";
    return this;
  }

  GasStationFilterBuilder equals(var pValue) {
    param.expression = "=";
    gasstationFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, gasstationFB.parameters, param, SqlSyntax.EQuals,
            gasstationFB._addedBlocks)
        : setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.NotEQuals, gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder isNull() {
    gasstationFB._addedBlocks = setCriteria(
        0,
        gasstationFB.parameters,
        param,
        SqlSyntax.IsNULL.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder contains(dynamic pValue) {
    gasstationFB._addedBlocks = setCriteria(
        "%" + pValue.toString() + "%",
        gasstationFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder startsWith(dynamic pValue) {
    gasstationFB._addedBlocks = setCriteria(
        pValue.toString() + "%",
        gasstationFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder endsWith(dynamic pValue) {
    gasstationFB._addedBlocks = setCriteria(
        "%" + pValue.toString(),
        gasstationFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder between(dynamic pFirst, dynamic pLast) {
    if (pFirst != null && pLast != null) {
      gasstationFB._addedBlocks = setCriteria(
          pFirst,
          gasstationFB.parameters,
          param,
          SqlSyntax.Between.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
          gasstationFB._addedBlocks,
          pLast);
    } else if (pFirst != null) {
      if (_waitingNot != "") {
        gasstationFB._addedBlocks = setCriteria(pFirst, gasstationFB.parameters,
            param, SqlSyntax.LessThan, gasstationFB._addedBlocks);
      } else {
        gasstationFB._addedBlocks = setCriteria(pFirst, gasstationFB.parameters,
            param, SqlSyntax.GreaterThanOrEquals, gasstationFB._addedBlocks);
      }
    } else if (pLast != null) {
      if (_waitingNot != "") {
        gasstationFB._addedBlocks = setCriteria(pLast, gasstationFB.parameters,
            param, SqlSyntax.GreaterThan, gasstationFB._addedBlocks);
      } else {
        gasstationFB._addedBlocks = setCriteria(pLast, gasstationFB.parameters,
            param, SqlSyntax.LessThanOrEquals, gasstationFB._addedBlocks);
      }
    }
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder greaterThan(dynamic pValue) {
    param.expression = ">";
    gasstationFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.GreaterThan, gasstationFB._addedBlocks)
        : setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.LessThanOrEquals, gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder lessThan(dynamic pValue) {
    param.expression = "<";
    gasstationFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.LessThan, gasstationFB._addedBlocks)
        : setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder greaterThanOrEquals(dynamic pValue) {
    param.expression = ">=";
    gasstationFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, gasstationFB._addedBlocks)
        : setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.LessThan, gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder lessThanOrEquals(dynamic pValue) {
    param.expression = "<=";
    gasstationFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.LessThanOrEquals, gasstationFB._addedBlocks)
        : setCriteria(pValue, gasstationFB.parameters, param,
            SqlSyntax.GreaterThan, gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }

  GasStationFilterBuilder inValues(var pValue) {
    gasstationFB._addedBlocks = setCriteria(
        pValue,
        gasstationFB.parameters,
        param,
        SqlSyntax.IN.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        gasstationFB._addedBlocks);
    _waitingNot = "";
    gasstationFB._addedBlocks.needEndBlock[gasstationFB._blockIndex] =
        gasstationFB._addedBlocks.retVal;
    return gasstationFB;
  }
}
// endregion GasStationField

// region GasStationFilterBuilder
class GasStationFilterBuilder extends SearchCriteria {
  GasStationFilterBuilder(GasStation obj) {
    whereString = "";
    qparams = QueryParams();
    parameters = List<DbParameter>();
    orderByList = List<String>();
    groupByList = List<String>();
    _addedBlocks = AddedBlocks(List<bool>(), List<bool>());
    _addedBlocks.needEndBlock.add(false);
    _addedBlocks.waitingStartBlock.add(false);
    _pagesize = 0;
    _page = 0;
    _obj = obj;
  }
  AddedBlocks _addedBlocks;
  int _blockIndex = 0;
  List<DbParameter> parameters;
  List<String> orderByList;
  GasStation _obj;
  QueryParams qparams;
  int _pagesize;
  int _page;

  GasStationFilterBuilder get and {
    if (parameters.isNotEmpty) {
      parameters[parameters.length - 1].wOperator = " AND ";
    }
    return this;
  }

  GasStationFilterBuilder get or {
    if (parameters.isNotEmpty) {
      parameters[parameters.length - 1].wOperator = " OR ";
    }
    return this;
  }

  GasStationFilterBuilder get startBlock {
    _addedBlocks.waitingStartBlock.add(true);
    _addedBlocks.needEndBlock.add(false);
    _blockIndex++;
    if (_blockIndex > 1) _addedBlocks.needEndBlock[_blockIndex - 1] = true;
    return this;
  }

  GasStationFilterBuilder where(String whereCriteria) {
    if (whereCriteria != null && whereCriteria != "") {
      final DbParameter param = DbParameter();
      _addedBlocks = setCriteria(
          0, parameters, param, "(" + whereCriteria + ")", _addedBlocks);
      _addedBlocks.needEndBlock[_blockIndex] = _addedBlocks.retVal;
    }
    return this;
  }

  GasStationFilterBuilder page(int page, int pagesize) {
    if (page > 0) _page = page;
    if (pagesize > 0) _pagesize = pagesize;
    return this;
  }

  GasStationFilterBuilder top(int count) {
    if (count > 0) {
      _pagesize = count;
    }
    return this;
  }

  GasStationFilterBuilder get endBlock {
    if (_addedBlocks.needEndBlock[_blockIndex]) {
      parameters[parameters.length - 1].whereString += " ) ";
    }
    _addedBlocks.needEndBlock.removeAt(_blockIndex);
    _addedBlocks.waitingStartBlock.removeAt(_blockIndex);
    _blockIndex--;
    return this;
  }

  GasStationFilterBuilder orderBy(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        orderByList.add(argFields);
      } else {
        for (String s in argFields) {
          if (s != null && s != "") orderByList.add(" $s ");
        }
      }
    }
    return this;
  }

  GasStationFilterBuilder orderByDesc(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        orderByList.add("$argFields desc ");
      } else {
        for (String s in argFields) {
          if (s != null && s != "") orderByList.add(" $s desc ");
        }
      }
    }
    return this;
  }

  GasStationFilterBuilder groupBy(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        groupByList.add(" $argFields ");
      } else {
        for (String s in argFields) {
          if (s != null && s != "") groupByList.add(" $s ");
        }
      }
    }
    return this;
  }

  GasStationField setField(
      GasStationField field, String colName, DbType dbtype) {
    field = GasStationField(this);
    field.param = DbParameter(
        dbType: dbtype,
        columnName: colName,
        wStartBlock: _addedBlocks.waitingStartBlock[_blockIndex]);
    return field;
  }

  GasStationField _id;
  GasStationField get id {
    _id = setField(_id, "id", DbType.integer);
    return _id;
  }

  GasStationField _name;
  GasStationField get name {
    _name = setField(_name, "name", DbType.text);
    return _name;
  }

  GasStationField _city;
  GasStationField get city {
    _city = setField(_city, "city", DbType.text);
    return _city;
  }

  bool _getIsDeleted;

  void _buildParameters() {
    if (_page > 0 && _pagesize > 0) {
      qparams.limit = _pagesize;
      qparams.offset = (_page - 1) * _pagesize;
    } else {
      qparams.limit = _pagesize;
      qparams.offset = _page;
    }
    for (DbParameter param in parameters) {
      if (param.columnName != null) {
        if (param.value is List) {
          param.value = param.value
              .toString()
              .replaceAll("[", "")
              .replaceAll("]", "")
              .toString();
          whereString += param.whereString
              .replaceAll("{field}", param.columnName)
              .replaceAll("?", param.value.toString());
          param.value = null;
        } else {
          whereString +=
              param.whereString.replaceAll("{field}", param.columnName);
        }
        switch (param.dbType) {
          case DbType.bool:
            if (param.value != null) param.value = param.value == true ? 1 : 0;
            break;
          default:
        }

        if (param.value != null) whereArguments.add(param.value);
        if (param.value2 != null) whereArguments.add(param.value2);
      } else {
        whereString += param.whereString;
      }
    }
    if (GasStation._softDeleteActivated) {
      if (whereString != "") {
        whereString = (!_getIsDeleted ? "ifnull(isDeleted,0)=0 AND" : "") +
            " ($whereString)";
      } else if (!_getIsDeleted) {
        whereString = "ifnull(isDeleted,0)=0";
      }
    }

    if (whereString != "") {
      qparams.whereString = whereString;
    }
    qparams.whereArguments = whereArguments;
    qparams.groupBy = groupByList.join(',');
    qparams.orderBy = orderByList.join(',');
  }

  /// <summary>
  /// Deletes List<GasStation> batch by query
  /// </summary>
  /// <returns>BoolResult res.success=Deleted, not res.success=Can not deleted</returns>
  Future<BoolResult> delete([bool hardDelete = false]) async {
    _buildParameters();
    var r = BoolResult();
    if (GasStation._softDeleteActivated && !hardDelete) {
      r = await _obj._mnGasStation.updateBatch(qparams, {"isDeleted": 1});
    } else {
      r = await _obj._mnGasStation.delete(qparams);
    }
    return r;
  }

  Future<BoolResult> update(Map<String, dynamic> values) {
    _buildParameters();
    return _obj._mnGasStation.updateBatch(qparams, values);
  }

  /// This method always returns GasStationObj if exist, otherwise returns null
  /// <returns>List<GasStation></returns>
  Future<GasStation> toSingle([VoidCallback gasstation(GasStation o)]) async {
    _pagesize = 1;
    _buildParameters();
    final objFuture = _obj._mnGasStation.toList(qparams);
    final data = await objFuture;
    GasStation retVal;
    if (data.isNotEmpty) {
      retVal = GasStation.fromMap(data[0] as Map<String, dynamic>);
    } else {
      retVal = null;
    }
    if (gasstation != null) {
      gasstation(retVal);
    }
    return retVal;
  }

  /// This method always returns int.
  /// <returns>int</returns>
  Future<BoolResult> toCount(VoidCallback gasstationCount(int c)) async {
    _buildParameters();
    qparams.selectColumns = ["COUNT(1) AS CNT"];
    final gasstationsFuture = await _obj._mnGasStation.toList(qparams);
    final int count = gasstationsFuture[0]["CNT"] as int;
    gasstationCount(count);
    return BoolResult(
        success: count > 0,
        successMessage: count > 0 ? "toCount(): $count items found" : "",
        errorMessage: count > 0 ? "" : "toCount(): no items found");
  }

  /// This method always returns List<GasStation>.
  /// <returns>List<GasStation></returns>
  Future<List<GasStation>> toList(
      [VoidCallback gasstationList(List<GasStation> o)]) async {
    _buildParameters();
    final gasstationsFuture = _obj._mnGasStation.toList(qparams);
    final List<GasStation> gasstationsData = List<GasStation>();
    final data = await gasstationsFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      gasstationsData.add(GasStation.fromMap(data[i] as Map<String, dynamic>));
    }
    if (gasstationList != null) gasstationList(gasstationsData);
    return gasstationsData;
  }

  /// Returns List<DropdownMenuItem<GasStation>>
  Future<List<DropdownMenuItem<GasStation>>> toDropDownMenu(
      String displayTextColumn,
      [VoidCallback dropDownMenu(List<DropdownMenuItem<GasStation>> o)]) async {
    _buildParameters();
    final gasstationsFuture = _obj._mnGasStation.toList(qparams);

    final data = await gasstationsFuture;
    final int count = data.length;
    final List<DropdownMenuItem<GasStation>> items = List();
    items.add(DropdownMenuItem(
      value: GasStation(),
      child: Text("Select GasStation"),
    ));
    for (int i = 0; i < count; i++) {
      items.add(
        DropdownMenuItem(
          value: GasStation.fromMap(data[i] as Map<String, dynamic>),
          child: Text(data[i][displayTextColumn].toString()),
        ),
      );
    }
    if (dropDownMenu != null) {
      dropDownMenu(items);
    }
    return items;
  }

  /// Returns List<DropdownMenuItem<int>>
  Future<List<DropdownMenuItem<int>>> toDropDownMenuInt(
      String displayTextColumn,
      [VoidCallback dropDownMenu(List<DropdownMenuItem<int>> o)]) async {
    _buildParameters();
    qparams.selectColumns = ["id", displayTextColumn];
    final gasstationsFuture = _obj._mnGasStation.toList(qparams);

    final data = await gasstationsFuture;
    final int count = data.length;
    final List<DropdownMenuItem<int>> items = List();
    items.add(DropdownMenuItem(
      value: 0,
      child: Text("Select GasStation"),
    ));
    for (int i = 0; i < count; i++) {
      items.add(
        DropdownMenuItem(
          value: data[i]["id"] as int,
          child: Text(data[i][displayTextColumn].toString()),
        ),
      );
    }
    if (dropDownMenu != null) {
      dropDownMenu(items);
    }
    return items;
  }

  /// This method always returns Primary Key List<int>.
  /// <returns>List<int></returns>
  Future<List<int>> toListPrimaryKey(
      [VoidCallback idList(List<int> o), bool buildParameters = true]) async {
    if (buildParameters) _buildParameters();
    final List<int> idData = List<int>();
    qparams.selectColumns = ["id"];
    final idFuture = await _obj._mnGasStation.toList(qparams);

    final int count = idFuture.length;
    for (int i = 0; i < count; i++) {
      idData.add(idFuture[i]["id"] as int);
    }
    if (idList != null) {
      idList(idData);
    }
    return idData;
  }

  /// Returns List<dynamic> for selected columns. Use this method for "groupBy" with min,max,avg..
  /// Sample usage: (see EXAMPLE 4.2 at https://github.com/hhtokpinar/sqfEntity#group-by)
  Future<List<dynamic>> toListObject(
      [VoidCallback listObject(List<dynamic> o)]) async {
    _buildParameters();

    final objectFuture = _obj._mnGasStation.toList(qparams);

    final List<dynamic> objectsData = List<dynamic>();
    final data = await objectFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      objectsData.add(data[i]);
    }
    if (listObject != null) {
      listObject(objectsData);
    }
    return objectsData;
  }

  /// Returns List<String> for selected first column
  /// Sample usage: await GasStation.select(columnsToSelect: ["columnName"]).toListString()
  Future<List<String>> toListString(
      [VoidCallback listString(List<String> o)]) async {
    _buildParameters();

    final objectFuture = _obj._mnGasStation.toList(qparams);

    final List<String> objectsData = List<String>();
    final data = await objectFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      objectsData.add(data[i][qparams.selectColumns[0]].toString());
    }
    if (listString != null) {
      listString(objectsData);
    }
    return objectsData;
  }
}
// endregion GasStationFilterBuilder

// region GasStationFields
class GasStationFields {
  static TableField _fId;
  static TableField get id {
    _fId = SqlSyntax.setField(_fId, "id", DbType.integer);
    return _fId;
  }

  static TableField _fName;
  static TableField get name {
    _fName = SqlSyntax.setField(_fName, "name", DbType.text);
    return _fName;
  }

  static TableField _fCity;
  static TableField get city {
    _fCity = SqlSyntax.setField(_fCity, "city", DbType.text);
    return _fCity;
  }
}
// endregion GasStationFields

//region GasStationManager
class GasStationManager extends SqfEntityProvider {
  GasStationManager()
      : super(MyDbModel(), tableName: _tableName, colId: _colId);
  static String _tableName = "gasStation";
  static String _colId = "id";
}
//endregion GasStationManager

/*
      These classes was generated by SqfEntity
      To use these SqfEntity classes do following: 
      - import Truck.dart into where to use
      - start typing Truck().select()... (add a few filters with fluent methods)...(add orderBy/orderBydesc if you want)...
      - and then just put end of filters / or end of only select()  toSingle(truck) / or toList(truckList) 
      - you can select one Truck or List<Truck> by your filters and orders
      - also you can batch update or batch delete by using delete/update methods instead of tosingle/tolist methods
        Enjoy.. Huseyin Tokpunar
      */
// region Truck
class Truck {
  Truck({this.id, this.truck, this.driver}) {
    setDefaultValues();
  }
  Truck.withFields(this.truck, this.driver) {
    setDefaultValues();
  }
  Truck.withId(this.id, this.truck, this.driver) {
    setDefaultValues();
  }
  Truck.fromMap(Map<String, dynamic> o) {
    id = o["id"] as int;
    truck = o["truck"] as String;
    driver = o["driver"] as String;
  }
  // FIELDS
  int id;
  String truck;
  String driver;
  // end FIELDS

// COLLECTIONS
  SupplyFilterBuilder getSupplies(
      {List<String> columnsToSelect, bool getIsDeleted}) {
    return Supply()
        .select(columnsToSelect: columnsToSelect, getIsDeleted: getIsDeleted)
        .truckId
        .equals(id);
  }
  // END COLLECTIONS

  static const bool _softDeleteActivated = false;
  TruckManager __mnTruck;
  TruckFilterBuilder _select;

  TruckManager get _mnTruck {
    if (__mnTruck == null) __mnTruck = TruckManager();
    return __mnTruck;
  }

  // methods
  Map<String, dynamic> toMap({bool forQuery = false}) {
    final map = Map<String, dynamic>();
    if (id != null) {
      map["id"] = id;
    }
    if (truck != null) {
      map["truck"] = truck;
    }
    if (driver != null) {
      map["driver"] = driver;
    }

    return map;
  }

  List<dynamic> toArgs() {
    return [id, truck, driver];
  }

  static Future<List<Truck>> fromWebUrl(String url,
      [VoidCallback truckList(List<Truck> o)]) async {
    var objList = List<Truck>();
    try {
      final response = await http.get(url);
      final Iterable list = json.decode(response.body) as Iterable;
      objList = list
          .map((truck) => Truck.fromMap(truck as Map<String, dynamic>))
          .toList();
      if (truckList != null) {
        truckList(objList);
      }
      return objList;
    } catch (e) {
      print("SQFENTITY ERROR Truck.fromWeb: ErrorMessage:" + e.toString());
      return null;
    }
  }

  static Future<List<Truck>> fromObjectList(Future<List<dynamic>> o) async {
    final trucksList = List<Truck>();
    final data = await o;
    for (int i = 0; i < data.length; i++) {
      trucksList.add(Truck.fromMap(data[i] as Map<String, dynamic>));
    }
    return trucksList;
  }

  static List<Truck> fromMapList(List<Map<String, dynamic>> query) {
    final List<Truck> trucks = List<Truck>();
    for (Map map in query) {
      trucks.add(Truck.fromMap(map as Map<String, dynamic>));
    }
    return trucks;
  }

  /// returns Truck by ID if exist, otherwise returns null
  /// <param name="id">Primary Key Value</param>
  /// <returns>returns Truck if exist, otherwise returns null</returns>
  Future<Truck> getById(int id) async {
    Truck truckObj;
    final data = await _mnTruck.getById(id);
    if (data.length != 0) {
      truckObj = Truck.fromMap(data[0] as Map<String, dynamic>);
    } else {
      truckObj = null;
    }
    return truckObj;
  }

  /// <summary>
  /// Saves the object. If the id field is null, saves as a new record and returns new id, if id is not null then updates record
  /// </summary>
  /// <returns>Returns id</returns>
  Future<int> save() async {
    if (id == null || id == 0) {
      id = await _mnTruck.insert(Truck.withFields(truck, driver));
    } else {
      id = await _upsert();
    }
    return id;
  }

  /// <summary>
  /// saveAll method saves the sent List<Truck> as a batch in one transaction
  /// </summary>
  /// <returns> Returns a <List<BoolResult>> </returns>
  Future<List<BoolResult>> saveAll(List<Truck> trucks) async {
    final results = _mnTruck.saveAll(
        "INSERT OR REPLACE INTO truck (id, truck,driver)  VALUES (?,?,?)",
        trucks);
    return results;
  }

  /// <summary>
  /// Updates if the record exists, otherwise adds a new row
  /// </summary>
  /// <returns>Returns id</returns>
  Future<int> _upsert() async {
    id = await _mnTruck.rawInsert(
        "INSERT OR REPLACE INTO truck (id, truck,driver)  VALUES (?,?,?)",
        [id, truck, driver]);
    return id;
  }

  /// <summary>
  /// inserts or replaces the sent List<Todo> as a batch in one transaction.
  /// upsertAll() method is faster then saveAll() method. upsertAll() should be used when you are sure that the primary key is greater than zero
  /// </summary>
  /// <returns> Returns a <List<BoolResult>> </returns>
  Future<List<BoolResult>> upsertAll(List<Truck> trucks) async {
    final results = await _mnTruck.rawInsertAll(
        "INSERT OR REPLACE INTO truck (id, truck,driver)  VALUES (?,?,?)",
        trucks);
    return results;
  }

  /// <summary>
  /// saveAs Truck. Returns a new Primary Key value of Truck
  /// </summary>
  /// <returns>Returns a new Primary Key value of Truck</returns>
  Future<int> saveAs() async {
    id = await _mnTruck.insert(Truck.withFields(truck, driver));
    return id;
  }

  /// <summary>
  /// Deletes Truck
  /// </summary>
  /// <returns>BoolResult res.success=Deleted, not res.success=Can not deleted</returns>
  Future<BoolResult> delete([bool hardDelete = false]) async {
    print("SQFENTITIY: delete Truck invoked (id=$id)");
    var result = BoolResult();
    {
      result = await Supply().select().truckId.equals(id).delete(hardDelete);
    }
    if (!result.success) {
      return result;
    } else if (!_softDeleteActivated || hardDelete) {
      return _mnTruck.delete(QueryParams(whereString: "id=$id"));
    } else {
      return _mnTruck
          .updateBatch(QueryParams(whereString: "id=$id"), {"isDeleted": 1});
    }
  }

  //private TruckFilterBuilder _Select;
  TruckFilterBuilder select({List<String> columnsToSelect, bool getIsDeleted}) {
    _select = TruckFilterBuilder(this);
    _select._getIsDeleted = getIsDeleted == true;
    _select.qparams.selectColumns = columnsToSelect;
    return _select;
  }

  TruckFilterBuilder distinct(
      {List<String> columnsToSelect, bool getIsDeleted}) {
    final TruckFilterBuilder _distinct = TruckFilterBuilder(this);
    _distinct._getIsDeleted = getIsDeleted == true;
    _distinct.qparams.selectColumns = columnsToSelect;
    _distinct.qparams.distinct = true;
    return _distinct;
  }

  void setDefaultValues() {}
  //end methods
}
// endregion truck

// region TruckField
class TruckField extends SearchCriteria {
  TruckField(this.truckFB) {
    param = DbParameter();
  }
  DbParameter param;
  String _waitingNot = "";
  TruckFilterBuilder truckFB;

  TruckField get not {
    _waitingNot = " NOT ";
    return this;
  }

  TruckFilterBuilder equals(var pValue) {
    param.expression = "=";
    truckFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, truckFB.parameters, param, SqlSyntax.EQuals,
            truckFB._addedBlocks)
        : setCriteria(pValue, truckFB.parameters, param, SqlSyntax.NotEQuals,
            truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder isNull() {
    truckFB._addedBlocks = setCriteria(
        0,
        truckFB.parameters,
        param,
        SqlSyntax.IsNULL.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder contains(dynamic pValue) {
    truckFB._addedBlocks = setCriteria(
        "%" + pValue.toString() + "%",
        truckFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder startsWith(dynamic pValue) {
    truckFB._addedBlocks = setCriteria(
        pValue.toString() + "%",
        truckFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder endsWith(dynamic pValue) {
    truckFB._addedBlocks = setCriteria(
        "%" + pValue.toString(),
        truckFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder between(dynamic pFirst, dynamic pLast) {
    if (pFirst != null && pLast != null) {
      truckFB._addedBlocks = setCriteria(
          pFirst,
          truckFB.parameters,
          param,
          SqlSyntax.Between.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
          truckFB._addedBlocks,
          pLast);
    } else if (pFirst != null) {
      if (_waitingNot != "") {
        truckFB._addedBlocks = setCriteria(pFirst, truckFB.parameters, param,
            SqlSyntax.LessThan, truckFB._addedBlocks);
      } else {
        truckFB._addedBlocks = setCriteria(pFirst, truckFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, truckFB._addedBlocks);
      }
    } else if (pLast != null) {
      if (_waitingNot != "") {
        truckFB._addedBlocks = setCriteria(pLast, truckFB.parameters, param,
            SqlSyntax.GreaterThan, truckFB._addedBlocks);
      } else {
        truckFB._addedBlocks = setCriteria(pLast, truckFB.parameters, param,
            SqlSyntax.LessThanOrEquals, truckFB._addedBlocks);
      }
    }
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder greaterThan(dynamic pValue) {
    param.expression = ">";
    truckFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, truckFB.parameters, param, SqlSyntax.GreaterThan,
            truckFB._addedBlocks)
        : setCriteria(pValue, truckFB.parameters, param,
            SqlSyntax.LessThanOrEquals, truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder lessThan(dynamic pValue) {
    param.expression = "<";
    truckFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, truckFB.parameters, param, SqlSyntax.LessThan,
            truckFB._addedBlocks)
        : setCriteria(pValue, truckFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder greaterThanOrEquals(dynamic pValue) {
    param.expression = ">=";
    truckFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, truckFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, truckFB._addedBlocks)
        : setCriteria(pValue, truckFB.parameters, param, SqlSyntax.LessThan,
            truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder lessThanOrEquals(dynamic pValue) {
    param.expression = "<=";
    truckFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, truckFB.parameters, param,
            SqlSyntax.LessThanOrEquals, truckFB._addedBlocks)
        : setCriteria(pValue, truckFB.parameters, param, SqlSyntax.GreaterThan,
            truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }

  TruckFilterBuilder inValues(var pValue) {
    truckFB._addedBlocks = setCriteria(
        pValue,
        truckFB.parameters,
        param,
        SqlSyntax.IN.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        truckFB._addedBlocks);
    _waitingNot = "";
    truckFB._addedBlocks.needEndBlock[truckFB._blockIndex] =
        truckFB._addedBlocks.retVal;
    return truckFB;
  }
}
// endregion TruckField

// region TruckFilterBuilder
class TruckFilterBuilder extends SearchCriteria {
  TruckFilterBuilder(Truck obj) {
    whereString = "";
    qparams = QueryParams();
    parameters = List<DbParameter>();
    orderByList = List<String>();
    groupByList = List<String>();
    _addedBlocks = AddedBlocks(List<bool>(), List<bool>());
    _addedBlocks.needEndBlock.add(false);
    _addedBlocks.waitingStartBlock.add(false);
    _pagesize = 0;
    _page = 0;
    _obj = obj;
  }
  AddedBlocks _addedBlocks;
  int _blockIndex = 0;
  List<DbParameter> parameters;
  List<String> orderByList;
  Truck _obj;
  QueryParams qparams;
  int _pagesize;
  int _page;

  TruckFilterBuilder get and {
    if (parameters.isNotEmpty) {
      parameters[parameters.length - 1].wOperator = " AND ";
    }
    return this;
  }

  TruckFilterBuilder get or {
    if (parameters.isNotEmpty) {
      parameters[parameters.length - 1].wOperator = " OR ";
    }
    return this;
  }

  TruckFilterBuilder get startBlock {
    _addedBlocks.waitingStartBlock.add(true);
    _addedBlocks.needEndBlock.add(false);
    _blockIndex++;
    if (_blockIndex > 1) _addedBlocks.needEndBlock[_blockIndex - 1] = true;
    return this;
  }

  TruckFilterBuilder where(String whereCriteria) {
    if (whereCriteria != null && whereCriteria != "") {
      final DbParameter param = DbParameter();
      _addedBlocks = setCriteria(
          0, parameters, param, "(" + whereCriteria + ")", _addedBlocks);
      _addedBlocks.needEndBlock[_blockIndex] = _addedBlocks.retVal;
    }
    return this;
  }

  TruckFilterBuilder page(int page, int pagesize) {
    if (page > 0) _page = page;
    if (pagesize > 0) _pagesize = pagesize;
    return this;
  }

  TruckFilterBuilder top(int count) {
    if (count > 0) {
      _pagesize = count;
    }
    return this;
  }

  TruckFilterBuilder get endBlock {
    if (_addedBlocks.needEndBlock[_blockIndex]) {
      parameters[parameters.length - 1].whereString += " ) ";
    }
    _addedBlocks.needEndBlock.removeAt(_blockIndex);
    _addedBlocks.waitingStartBlock.removeAt(_blockIndex);
    _blockIndex--;
    return this;
  }

  TruckFilterBuilder orderBy(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        orderByList.add(argFields);
      } else {
        for (String s in argFields) {
          if (s != null && s != "") orderByList.add(" $s ");
        }
      }
    }
    return this;
  }

  TruckFilterBuilder orderByDesc(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        orderByList.add("$argFields desc ");
      } else {
        for (String s in argFields) {
          if (s != null && s != "") orderByList.add(" $s desc ");
        }
      }
    }
    return this;
  }

  TruckFilterBuilder groupBy(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        groupByList.add(" $argFields ");
      } else {
        for (String s in argFields) {
          if (s != null && s != "") groupByList.add(" $s ");
        }
      }
    }
    return this;
  }

  TruckField setField(TruckField field, String colName, DbType dbtype) {
    field = TruckField(this);
    field.param = DbParameter(
        dbType: dbtype,
        columnName: colName,
        wStartBlock: _addedBlocks.waitingStartBlock[_blockIndex]);
    return field;
  }

  TruckField _id;
  TruckField get id {
    _id = setField(_id, "id", DbType.integer);
    return _id;
  }

  TruckField _truck;
  TruckField get truck {
    _truck = setField(_truck, "truck", DbType.text);
    return _truck;
  }

  TruckField _driver;
  TruckField get driver {
    _driver = setField(_driver, "driver", DbType.text);
    return _driver;
  }

  bool _getIsDeleted;

  void _buildParameters() {
    if (_page > 0 && _pagesize > 0) {
      qparams.limit = _pagesize;
      qparams.offset = (_page - 1) * _pagesize;
    } else {
      qparams.limit = _pagesize;
      qparams.offset = _page;
    }
    for (DbParameter param in parameters) {
      if (param.columnName != null) {
        if (param.value is List) {
          param.value = param.value
              .toString()
              .replaceAll("[", "")
              .replaceAll("]", "")
              .toString();
          whereString += param.whereString
              .replaceAll("{field}", param.columnName)
              .replaceAll("?", param.value.toString());
          param.value = null;
        } else {
          whereString +=
              param.whereString.replaceAll("{field}", param.columnName);
        }
        switch (param.dbType) {
          case DbType.bool:
            if (param.value != null) param.value = param.value == true ? 1 : 0;
            break;
          default:
        }

        if (param.value != null) whereArguments.add(param.value);
        if (param.value2 != null) whereArguments.add(param.value2);
      } else {
        whereString += param.whereString;
      }
    }
    if (Truck._softDeleteActivated) {
      if (whereString != "") {
        whereString = (!_getIsDeleted ? "ifnull(isDeleted,0)=0 AND" : "") +
            " ($whereString)";
      } else if (!_getIsDeleted) {
        whereString = "ifnull(isDeleted,0)=0";
      }
    }

    if (whereString != "") {
      qparams.whereString = whereString;
    }
    qparams.whereArguments = whereArguments;
    qparams.groupBy = groupByList.join(',');
    qparams.orderBy = orderByList.join(',');
  }

  /// <summary>
  /// Deletes List<Truck> batch by query
  /// </summary>
  /// <returns>BoolResult res.success=Deleted, not res.success=Can not deleted</returns>
  Future<BoolResult> delete([bool hardDelete = false]) async {
    _buildParameters();
    var r = BoolResult();
    if (Truck._softDeleteActivated && !hardDelete) {
      r = await _obj._mnTruck.updateBatch(qparams, {"isDeleted": 1});
    } else {
      r = await _obj._mnTruck.delete(qparams);
    }
    return r;
  }

  Future<BoolResult> update(Map<String, dynamic> values) {
    _buildParameters();
    return _obj._mnTruck.updateBatch(qparams, values);
  }

  /// This method always returns TruckObj if exist, otherwise returns null
  /// <returns>List<Truck></returns>
  Future<Truck> toSingle([VoidCallback truck(Truck o)]) async {
    _pagesize = 1;
    _buildParameters();
    final objFuture = _obj._mnTruck.toList(qparams);
    final data = await objFuture;
    Truck retVal;
    if (data.isNotEmpty) {
      retVal = Truck.fromMap(data[0] as Map<String, dynamic>);
    } else {
      retVal = null;
    }
    if (truck != null) {
      truck(retVal);
    }
    return retVal;
  }

  /// This method always returns int.
  /// <returns>int</returns>
  Future<BoolResult> toCount(VoidCallback truckCount(int c)) async {
    _buildParameters();
    qparams.selectColumns = ["COUNT(1) AS CNT"];
    final trucksFuture = await _obj._mnTruck.toList(qparams);
    final int count = trucksFuture[0]["CNT"] as int;
    truckCount(count);
    return BoolResult(
        success: count > 0,
        successMessage: count > 0 ? "toCount(): $count items found" : "",
        errorMessage: count > 0 ? "" : "toCount(): no items found");
  }

  /// This method always returns List<Truck>.
  /// <returns>List<Truck></returns>
  Future<List<Truck>> toList([VoidCallback truckList(List<Truck> o)]) async {
    _buildParameters();
    final trucksFuture = _obj._mnTruck.toList(qparams);
    final List<Truck> trucksData = List<Truck>();
    final data = await trucksFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      trucksData.add(Truck.fromMap(data[i] as Map<String, dynamic>));
    }
    if (truckList != null) truckList(trucksData);
    return trucksData;
  }

  /// Returns List<DropdownMenuItem<Truck>>
  Future<List<DropdownMenuItem<Truck>>> toDropDownMenu(String displayTextColumn,
      [VoidCallback dropDownMenu(List<DropdownMenuItem<Truck>> o)]) async {
    _buildParameters();
    final trucksFuture = _obj._mnTruck.toList(qparams);

    final data = await trucksFuture;
    final int count = data.length;
    final List<DropdownMenuItem<Truck>> items = List();
    items.add(DropdownMenuItem(
      value: Truck(),
      child: Text("Select Truck"),
    ));
    for (int i = 0; i < count; i++) {
      items.add(
        DropdownMenuItem(
          value: Truck.fromMap(data[i] as Map<String, dynamic>),
          child: Text(data[i][displayTextColumn].toString()),
        ),
      );
    }
    if (dropDownMenu != null) {
      dropDownMenu(items);
    }
    return items;
  }

  /// Returns List<DropdownMenuItem<int>>
  Future<List<DropdownMenuItem<int>>> toDropDownMenuInt(
      String displayTextColumn,
      [VoidCallback dropDownMenu(List<DropdownMenuItem<int>> o)]) async {
    _buildParameters();
    qparams.selectColumns = ["id", displayTextColumn];
    final trucksFuture = _obj._mnTruck.toList(qparams);

    final data = await trucksFuture;
    final int count = data.length;
    final List<DropdownMenuItem<int>> items = List();
    items.add(DropdownMenuItem(
      value: 0,
      child: Text("Select Truck"),
    ));
    for (int i = 0; i < count; i++) {
      items.add(
        DropdownMenuItem(
          value: data[i]["id"] as int,
          child: Text(data[i][displayTextColumn].toString()),
        ),
      );
    }
    if (dropDownMenu != null) {
      dropDownMenu(items);
    }
    return items;
  }

  /// This method always returns Primary Key List<int>.
  /// <returns>List<int></returns>
  Future<List<int>> toListPrimaryKey(
      [VoidCallback idList(List<int> o), bool buildParameters = true]) async {
    if (buildParameters) _buildParameters();
    final List<int> idData = List<int>();
    qparams.selectColumns = ["id"];
    final idFuture = await _obj._mnTruck.toList(qparams);

    final int count = idFuture.length;
    for (int i = 0; i < count; i++) {
      idData.add(idFuture[i]["id"] as int);
    }
    if (idList != null) {
      idList(idData);
    }
    return idData;
  }

  /// Returns List<dynamic> for selected columns. Use this method for "groupBy" with min,max,avg..
  /// Sample usage: (see EXAMPLE 4.2 at https://github.com/hhtokpinar/sqfEntity#group-by)
  Future<List<dynamic>> toListObject(
      [VoidCallback listObject(List<dynamic> o)]) async {
    _buildParameters();

    final objectFuture = _obj._mnTruck.toList(qparams);

    final List<dynamic> objectsData = List<dynamic>();
    final data = await objectFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      objectsData.add(data[i]);
    }
    if (listObject != null) {
      listObject(objectsData);
    }
    return objectsData;
  }

  /// Returns List<String> for selected first column
  /// Sample usage: await Truck.select(columnsToSelect: ["columnName"]).toListString()
  Future<List<String>> toListString(
      [VoidCallback listString(List<String> o)]) async {
    _buildParameters();

    final objectFuture = _obj._mnTruck.toList(qparams);

    final List<String> objectsData = List<String>();
    final data = await objectFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      objectsData.add(data[i][qparams.selectColumns[0]].toString());
    }
    if (listString != null) {
      listString(objectsData);
    }
    return objectsData;
  }
}
// endregion TruckFilterBuilder

// region TruckFields
class TruckFields {
  static TableField _fId;
  static TableField get id {
    _fId = SqlSyntax.setField(_fId, "id", DbType.integer);
    return _fId;
  }

  static TableField _fTruck;
  static TableField get truck {
    _fTruck = SqlSyntax.setField(_fTruck, "truck", DbType.text);
    return _fTruck;
  }

  static TableField _fDriver;
  static TableField get driver {
    _fDriver = SqlSyntax.setField(_fDriver, "driver", DbType.text);
    return _fDriver;
  }
}
// endregion TruckFields

//region TruckManager
class TruckManager extends SqfEntityProvider {
  TruckManager() : super(MyDbModel(), tableName: _tableName, colId: _colId);
  static String _tableName = "truck";
  static String _colId = "id";
}
//endregion TruckManager

/*
      These classes was generated by SqfEntity
      To use these SqfEntity classes do following: 
      - import Supply.dart into where to use
      - start typing Supply().select()... (add a few filters with fluent methods)...(add orderBy/orderBydesc if you want)...
      - and then just put end of filters / or end of only select()  toSingle(supply) / or toList(supplyList) 
      - you can select one Supply or List<Supply> by your filters and orders
      - also you can batch update or batch delete by using delete/update methods instead of tosingle/tolist methods
        Enjoy.. Huseyin Tokpunar
      */
// region Supply
class Supply {
  Supply(
      {this.id,
      this.name,
      this.date,
      this.kmOld,
      this.kmNow,
      this.qtyLiters,
      this.avg,
      this.expense,
      this.gainGross,
      this.gainNet,
      this.gainKm,
      this.truckId,
      this.gasStationId}) {
    setDefaultValues();
  }
  Supply.withFields(
      this.name,
      this.date,
      this.kmOld,
      this.kmNow,
      this.qtyLiters,
      this.avg,
      this.expense,
      this.gainGross,
      this.gainNet,
      this.gainKm,
      this.truckId,
      this.gasStationId) {
    setDefaultValues();
  }
  Supply.withId(
      this.id,
      this.name,
      this.date,
      this.kmOld,
      this.kmNow,
      this.qtyLiters,
      this.avg,
      this.expense,
      this.gainGross,
      this.gainNet,
      this.gainKm,
      this.truckId,
      this.gasStationId) {
    setDefaultValues();
  }
  Supply.fromMap(Map<String, dynamic> o) {
    id = o["id"] as int;
    name = o["name"] as String;
    date = o["date"] as int;
    kmOld = o["kmOld"] as double;
    kmNow = o["kmNow"] as double;
    qtyLiters = o["qtyLiters"] as double;
    avg = o["avg"] as double;
    expense = o["expense"] as double;
    gainGross = o["gainGross"] as double;
    gainNet = o["gainNet"] as double;
    gainKm = o["gainKm"] as double;
    truckId = o["truckId"] as int;
    gasStationId = o["gasStationId"] as int;
  }
  // FIELDS
  int id;
  String name;
  int date;
  double kmOld;
  double kmNow;
  double qtyLiters;
  double avg;
  double expense;
  double gainGross;
  double gainNet;
  double gainKm;
  int truckId;
  int gasStationId;
  // end FIELDS

// RELATIONSHIPS
  Future<Truck> getTruck([VoidCallback truck(Truck o)]) async {
    final obj = await Truck().getById(truckId);
    if (truck != null) {
      truck(obj);
    }
    return obj;
  }

  Future<GasStation> getGasStation(
      [VoidCallback gasstation(GasStation o)]) async {
    final obj = await GasStation().getById(gasStationId);
    if (gasstation != null) {
      gasstation(obj);
    }
    return obj;
  }
  // END RELATIONSHIPS

  static const bool _softDeleteActivated = false;
  SupplyManager __mnSupply;
  SupplyFilterBuilder _select;

  SupplyManager get _mnSupply {
    if (__mnSupply == null) __mnSupply = SupplyManager();
    return __mnSupply;
  }

  // methods
  Map<String, dynamic> toMap({bool forQuery = false}) {
    final map = Map<String, dynamic>();
    if (id != null) {
      map["id"] = id;
    }
    if (name != null) {
      map["name"] = name;
    }
    if (date != null) {
      map["date"] = date;
    }
    if (kmOld != null) {
      map["kmOld"] = kmOld;
    }
    if (kmNow != null) {
      map["kmNow"] = kmNow;
    }
    if (qtyLiters != null) {
      map["qtyLiters"] = qtyLiters;
    }
    if (avg != null) {
      map["avg"] = avg;
    }
    if (expense != null) {
      map["expense"] = expense;
    }
    if (gainGross != null) {
      map["gainGross"] = gainGross;
    }
    if (gainNet != null) {
      map["gainNet"] = gainNet;
    }
    if (gainKm != null) {
      map["gainKm"] = gainKm;
    }
    if (truckId != null) {
      map["truckId"] = truckId;
    }
    if (gasStationId != null) {
      map["gasStationId"] = gasStationId;
    }

    return map;
  }

  List<dynamic> toArgs() {
    return [
      id,
      name,
      date,
      kmOld,
      kmNow,
      qtyLiters,
      avg,
      expense,
      gainGross,
      gainNet,
      gainKm,
      truckId,
      gasStationId
    ];
  }

  static Future<List<Supply>> fromWebUrl(String url,
      [VoidCallback supplyList(List<Supply> o)]) async {
    var objList = List<Supply>();
    try {
      final response = await http.get(url);
      final Iterable list = json.decode(response.body) as Iterable;
      objList = list
          .map((supply) => Supply.fromMap(supply as Map<String, dynamic>))
          .toList();
      if (supplyList != null) {
        supplyList(objList);
      }
      return objList;
    } catch (e) {
      print("SQFENTITY ERROR Supply.fromWeb: ErrorMessage:" + e.toString());
      return null;
    }
  }

  static Future<List<Supply>> fromObjectList(Future<List<dynamic>> o) async {
    final supplysList = List<Supply>();
    final data = await o;
    for (int i = 0; i < data.length; i++) {
      supplysList.add(Supply.fromMap(data[i] as Map<String, dynamic>));
    }
    return supplysList;
  }

  static List<Supply> fromMapList(List<Map<String, dynamic>> query) {
    final List<Supply> supplys = List<Supply>();
    for (Map map in query) {
      supplys.add(Supply.fromMap(map as Map<String, dynamic>));
    }
    return supplys;
  }

  /// returns Supply by ID if exist, otherwise returns null
  /// <param name="id">Primary Key Value</param>
  /// <returns>returns Supply if exist, otherwise returns null</returns>
  Future<Supply> getById(int id) async {
    Supply supplyObj;
    final data = await _mnSupply.getById(id);
    if (data.length != 0) {
      supplyObj = Supply.fromMap(data[0] as Map<String, dynamic>);
    } else {
      supplyObj = null;
    }
    return supplyObj;
  }

  /// <summary>
  /// Saves the object. If the id field is null, saves as a new record and returns new id, if id is not null then updates record
  /// </summary>
  /// <returns>Returns id</returns>
  Future<int> save() async {
    if (id == null || id == 0) {
      id = await _mnSupply.insert(Supply.withFields(
          name,
          date,
          kmOld,
          kmNow,
          qtyLiters,
          avg,
          expense,
          gainGross,
          gainNet,
          gainKm,
          truckId,
          gasStationId));
    } else {
      id = await _upsert();
    }
    return id;
  }

  /// <summary>
  /// saveAll method saves the sent List<Supply> as a batch in one transaction
  /// </summary>
  /// <returns> Returns a <List<BoolResult>> </returns>
  Future<List<BoolResult>> saveAll(List<Supply> supplies) async {
    final results = _mnSupply.saveAll(
        "INSERT OR REPLACE INTO supply (id, name,date,kmOld,kmNow,qtyLiters,avg,expense,gainGross,gainNet,gainKm,truckId,gasStationId)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
        supplies);
    return results;
  }

  /// <summary>
  /// Updates if the record exists, otherwise adds a new row
  /// </summary>
  /// <returns>Returns id</returns>
  Future<int> _upsert() async {
    id = await _mnSupply.rawInsert(
        "INSERT OR REPLACE INTO supply (id, name,date,kmOld,kmNow,qtyLiters,avg,expense,gainGross,gainNet,gainKm,truckId,gasStationId)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
        [
          id,
          name,
          date,
          kmOld,
          kmNow,
          qtyLiters,
          avg,
          expense,
          gainGross,
          gainNet,
          gainKm,
          truckId,
          gasStationId
        ]);
    return id;
  }

  /// <summary>
  /// inserts or replaces the sent List<Todo> as a batch in one transaction.
  /// upsertAll() method is faster then saveAll() method. upsertAll() should be used when you are sure that the primary key is greater than zero
  /// </summary>
  /// <returns> Returns a <List<BoolResult>> </returns>
  Future<List<BoolResult>> upsertAll(List<Supply> supplies) async {
    final results = await _mnSupply.rawInsertAll(
        "INSERT OR REPLACE INTO supply (id, name,date,kmOld,kmNow,qtyLiters,avg,expense,gainGross,gainNet,gainKm,truckId,gasStationId)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
        supplies);
    return results;
  }

  /// <summary>
  /// saveAs Supply. Returns a new Primary Key value of Supply
  /// </summary>
  /// <returns>Returns a new Primary Key value of Supply</returns>
  Future<int> saveAs() async {
    id = await _mnSupply.insert(Supply.withFields(
        name,
        date,
        kmOld,
        kmNow,
        qtyLiters,
        avg,
        expense,
        gainGross,
        gainNet,
        gainKm,
        truckId,
        gasStationId));
    return id;
  }

  /// <summary>
  /// Deletes Supply
  /// </summary>
  /// <returns>BoolResult res.success=Deleted, not res.success=Can not deleted</returns>
  Future<BoolResult> delete([bool hardDelete = false]) async {
    print("SQFENTITIY: delete Supply invoked (id=$id)");
    if (!_softDeleteActivated || hardDelete) {
      return _mnSupply.delete(QueryParams(whereString: "id=$id"));
    } else {
      return _mnSupply
          .updateBatch(QueryParams(whereString: "id=$id"), {"isDeleted": 1});
    }
  }

  //private SupplyFilterBuilder _Select;
  SupplyFilterBuilder select(
      {List<String> columnsToSelect, bool getIsDeleted}) {
    _select = SupplyFilterBuilder(this);
    _select._getIsDeleted = getIsDeleted == true;
    _select.qparams.selectColumns = columnsToSelect;
    return _select;
  }

  SupplyFilterBuilder distinct(
      {List<String> columnsToSelect, bool getIsDeleted}) {
    final SupplyFilterBuilder _distinct = SupplyFilterBuilder(this);
    _distinct._getIsDeleted = getIsDeleted == true;
    _distinct.qparams.selectColumns = columnsToSelect;
    _distinct.qparams.distinct = true;
    return _distinct;
  }

  void setDefaultValues() {
    if (truckId == null) truckId = 0;
    if (gasStationId == null) gasStationId = 0;
  }
  //end methods
}
// endregion supply

// region SupplyField
class SupplyField extends SearchCriteria {
  SupplyField(this.supplyFB) {
    param = DbParameter();
  }
  DbParameter param;
  String _waitingNot = "";
  SupplyFilterBuilder supplyFB;

  SupplyField get not {
    _waitingNot = " NOT ";
    return this;
  }

  SupplyFilterBuilder equals(var pValue) {
    param.expression = "=";
    supplyFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, supplyFB.parameters, param, SqlSyntax.EQuals,
            supplyFB._addedBlocks)
        : setCriteria(pValue, supplyFB.parameters, param, SqlSyntax.NotEQuals,
            supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder isNull() {
    supplyFB._addedBlocks = setCriteria(
        0,
        supplyFB.parameters,
        param,
        SqlSyntax.IsNULL.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder contains(dynamic pValue) {
    supplyFB._addedBlocks = setCriteria(
        "%" + pValue.toString() + "%",
        supplyFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder startsWith(dynamic pValue) {
    supplyFB._addedBlocks = setCriteria(
        pValue.toString() + "%",
        supplyFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder endsWith(dynamic pValue) {
    supplyFB._addedBlocks = setCriteria(
        "%" + pValue.toString(),
        supplyFB.parameters,
        param,
        SqlSyntax.Contains.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder between(dynamic pFirst, dynamic pLast) {
    if (pFirst != null && pLast != null) {
      supplyFB._addedBlocks = setCriteria(
          pFirst,
          supplyFB.parameters,
          param,
          SqlSyntax.Between.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
          supplyFB._addedBlocks,
          pLast);
    } else if (pFirst != null) {
      if (_waitingNot != "") {
        supplyFB._addedBlocks = setCriteria(pFirst, supplyFB.parameters, param,
            SqlSyntax.LessThan, supplyFB._addedBlocks);
      } else {
        supplyFB._addedBlocks = setCriteria(pFirst, supplyFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, supplyFB._addedBlocks);
      }
    } else if (pLast != null) {
      if (_waitingNot != "") {
        supplyFB._addedBlocks = setCriteria(pLast, supplyFB.parameters, param,
            SqlSyntax.GreaterThan, supplyFB._addedBlocks);
      } else {
        supplyFB._addedBlocks = setCriteria(pLast, supplyFB.parameters, param,
            SqlSyntax.LessThanOrEquals, supplyFB._addedBlocks);
      }
    }
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder greaterThan(dynamic pValue) {
    param.expression = ">";
    supplyFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, supplyFB.parameters, param, SqlSyntax.GreaterThan,
            supplyFB._addedBlocks)
        : setCriteria(pValue, supplyFB.parameters, param,
            SqlSyntax.LessThanOrEquals, supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder lessThan(dynamic pValue) {
    param.expression = "<";
    supplyFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, supplyFB.parameters, param, SqlSyntax.LessThan,
            supplyFB._addedBlocks)
        : setCriteria(pValue, supplyFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder greaterThanOrEquals(dynamic pValue) {
    param.expression = ">=";
    supplyFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, supplyFB.parameters, param,
            SqlSyntax.GreaterThanOrEquals, supplyFB._addedBlocks)
        : setCriteria(pValue, supplyFB.parameters, param, SqlSyntax.LessThan,
            supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder lessThanOrEquals(dynamic pValue) {
    param.expression = "<=";
    supplyFB._addedBlocks = _waitingNot == ""
        ? setCriteria(pValue, supplyFB.parameters, param,
            SqlSyntax.LessThanOrEquals, supplyFB._addedBlocks)
        : setCriteria(pValue, supplyFB.parameters, param, SqlSyntax.GreaterThan,
            supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }

  SupplyFilterBuilder inValues(var pValue) {
    supplyFB._addedBlocks = setCriteria(
        pValue,
        supplyFB.parameters,
        param,
        SqlSyntax.IN.replaceAll(SqlSyntax.NOT_KEYWORD, _waitingNot),
        supplyFB._addedBlocks);
    _waitingNot = "";
    supplyFB._addedBlocks.needEndBlock[supplyFB._blockIndex] =
        supplyFB._addedBlocks.retVal;
    return supplyFB;
  }
}
// endregion SupplyField

// region SupplyFilterBuilder
class SupplyFilterBuilder extends SearchCriteria {
  SupplyFilterBuilder(Supply obj) {
    whereString = "";
    qparams = QueryParams();
    parameters = List<DbParameter>();
    orderByList = List<String>();
    groupByList = List<String>();
    _addedBlocks = AddedBlocks(List<bool>(), List<bool>());
    _addedBlocks.needEndBlock.add(false);
    _addedBlocks.waitingStartBlock.add(false);
    _pagesize = 0;
    _page = 0;
    _obj = obj;
  }
  AddedBlocks _addedBlocks;
  int _blockIndex = 0;
  List<DbParameter> parameters;
  List<String> orderByList;
  Supply _obj;
  QueryParams qparams;
  int _pagesize;
  int _page;

  SupplyFilterBuilder get and {
    if (parameters.isNotEmpty) {
      parameters[parameters.length - 1].wOperator = " AND ";
    }
    return this;
  }

  SupplyFilterBuilder get or {
    if (parameters.isNotEmpty) {
      parameters[parameters.length - 1].wOperator = " OR ";
    }
    return this;
  }

  SupplyFilterBuilder get startBlock {
    _addedBlocks.waitingStartBlock.add(true);
    _addedBlocks.needEndBlock.add(false);
    _blockIndex++;
    if (_blockIndex > 1) _addedBlocks.needEndBlock[_blockIndex - 1] = true;
    return this;
  }

  SupplyFilterBuilder where(String whereCriteria) {
    if (whereCriteria != null && whereCriteria != "") {
      final DbParameter param = DbParameter();
      _addedBlocks = setCriteria(
          0, parameters, param, "(" + whereCriteria + ")", _addedBlocks);
      _addedBlocks.needEndBlock[_blockIndex] = _addedBlocks.retVal;
    }
    return this;
  }

  SupplyFilterBuilder page(int page, int pagesize) {
    if (page > 0) _page = page;
    if (pagesize > 0) _pagesize = pagesize;
    return this;
  }

  SupplyFilterBuilder top(int count) {
    if (count > 0) {
      _pagesize = count;
    }
    return this;
  }

  SupplyFilterBuilder get endBlock {
    if (_addedBlocks.needEndBlock[_blockIndex]) {
      parameters[parameters.length - 1].whereString += " ) ";
    }
    _addedBlocks.needEndBlock.removeAt(_blockIndex);
    _addedBlocks.waitingStartBlock.removeAt(_blockIndex);
    _blockIndex--;
    return this;
  }

  SupplyFilterBuilder orderBy(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        orderByList.add(argFields);
      } else {
        for (String s in argFields) {
          if (s != null && s != "") orderByList.add(" $s ");
        }
      }
    }
    return this;
  }

  SupplyFilterBuilder orderByDesc(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        orderByList.add("$argFields desc ");
      } else {
        for (String s in argFields) {
          if (s != null && s != "") orderByList.add(" $s desc ");
        }
      }
    }
    return this;
  }

  SupplyFilterBuilder groupBy(var argFields) {
    if (argFields != null) {
      if (argFields is String) {
        groupByList.add(" $argFields ");
      } else {
        for (String s in argFields) {
          if (s != null && s != "") groupByList.add(" $s ");
        }
      }
    }
    return this;
  }

  SupplyField setField(SupplyField field, String colName, DbType dbtype) {
    field = SupplyField(this);
    field.param = DbParameter(
        dbType: dbtype,
        columnName: colName,
        wStartBlock: _addedBlocks.waitingStartBlock[_blockIndex]);
    return field;
  }

  SupplyField _id;
  SupplyField get id {
    _id = setField(_id, "id", DbType.integer);
    return _id;
  }

  SupplyField _name;
  SupplyField get name {
    _name = setField(_name, "name", DbType.text);
    return _name;
  }

  SupplyField _date;
  SupplyField get date {
    _date = setField(_date, "date", DbType.integer);
    return _date;
  }

  SupplyField _kmOld;
  SupplyField get kmOld {
    _kmOld = setField(_kmOld, "kmOld", DbType.real);
    return _kmOld;
  }

  SupplyField _kmNow;
  SupplyField get kmNow {
    _kmNow = setField(_kmNow, "kmNow", DbType.real);
    return _kmNow;
  }

  SupplyField _qtyLiters;
  SupplyField get qtyLiters {
    _qtyLiters = setField(_qtyLiters, "qtyLiters", DbType.real);
    return _qtyLiters;
  }

  SupplyField _avg;
  SupplyField get avg {
    _avg = setField(_avg, "avg", DbType.real);
    return _avg;
  }

  SupplyField _expense;
  SupplyField get expense {
    _expense = setField(_expense, "expense", DbType.real);
    return _expense;
  }

  SupplyField _gainGross;
  SupplyField get gainGross {
    _gainGross = setField(_gainGross, "gainGross", DbType.real);
    return _gainGross;
  }

  SupplyField _gainNet;
  SupplyField get gainNet {
    _gainNet = setField(_gainNet, "gainNet", DbType.real);
    return _gainNet;
  }

  SupplyField _gainKm;
  SupplyField get gainKm {
    _gainKm = setField(_gainKm, "gainKm", DbType.real);
    return _gainKm;
  }

  SupplyField _truckId;
  SupplyField get truckId {
    _truckId = setField(_truckId, "truckId", DbType.integer);
    return _truckId;
  }

  SupplyField _gasStationId;
  SupplyField get gasStationId {
    _gasStationId = setField(_gasStationId, "gasStationId", DbType.integer);
    return _gasStationId;
  }

  bool _getIsDeleted;

  void _buildParameters() {
    if (_page > 0 && _pagesize > 0) {
      qparams.limit = _pagesize;
      qparams.offset = (_page - 1) * _pagesize;
    } else {
      qparams.limit = _pagesize;
      qparams.offset = _page;
    }
    for (DbParameter param in parameters) {
      if (param.columnName != null) {
        if (param.value is List) {
          param.value = param.value
              .toString()
              .replaceAll("[", "")
              .replaceAll("]", "")
              .toString();
          whereString += param.whereString
              .replaceAll("{field}", param.columnName)
              .replaceAll("?", param.value.toString());
          param.value = null;
        } else {
          whereString +=
              param.whereString.replaceAll("{field}", param.columnName);
        }
        switch (param.dbType) {
          case DbType.bool:
            if (param.value != null) param.value = param.value == true ? 1 : 0;
            break;
          default:
        }

        if (param.value != null) whereArguments.add(param.value);
        if (param.value2 != null) whereArguments.add(param.value2);
      } else {
        whereString += param.whereString;
      }
    }
    if (Supply._softDeleteActivated) {
      if (whereString != "") {
        whereString = (!_getIsDeleted ? "ifnull(isDeleted,0)=0 AND" : "") +
            " ($whereString)";
      } else if (!_getIsDeleted) {
        whereString = "ifnull(isDeleted,0)=0";
      }
    }

    if (whereString != "") {
      qparams.whereString = whereString;
    }
    qparams.whereArguments = whereArguments;
    qparams.groupBy = groupByList.join(',');
    qparams.orderBy = orderByList.join(',');
  }

  /// <summary>
  /// Deletes List<Supply> batch by query
  /// </summary>
  /// <returns>BoolResult res.success=Deleted, not res.success=Can not deleted</returns>
  Future<BoolResult> delete([bool hardDelete = false]) async {
    _buildParameters();
    var r = BoolResult();
    if (Supply._softDeleteActivated && !hardDelete) {
      r = await _obj._mnSupply.updateBatch(qparams, {"isDeleted": 1});
    } else {
      r = await _obj._mnSupply.delete(qparams);
    }
    return r;
  }

  Future<BoolResult> update(Map<String, dynamic> values) {
    _buildParameters();
    return _obj._mnSupply.updateBatch(qparams, values);
  }

  /// This method always returns SupplyObj if exist, otherwise returns null
  /// <returns>List<Supply></returns>
  Future<Supply> toSingle([VoidCallback supply(Supply o)]) async {
    _pagesize = 1;
    _buildParameters();
    final objFuture = _obj._mnSupply.toList(qparams);
    final data = await objFuture;
    Supply retVal;
    if (data.isNotEmpty) {
      retVal = Supply.fromMap(data[0] as Map<String, dynamic>);
    } else {
      retVal = null;
    }
    if (supply != null) {
      supply(retVal);
    }
    return retVal;
  }

  /// This method always returns int.
  /// <returns>int</returns>
  Future<BoolResult> toCount(VoidCallback supplyCount(int c)) async {
    _buildParameters();
    qparams.selectColumns = ["COUNT(1) AS CNT"];
    final suppliesFuture = await _obj._mnSupply.toList(qparams);
    final int count = suppliesFuture[0]["CNT"] as int;
    supplyCount(count);
    return BoolResult(
        success: count > 0,
        successMessage: count > 0 ? "toCount(): $count items found" : "",
        errorMessage: count > 0 ? "" : "toCount(): no items found");
  }

  /// This method always returns List<Supply>.
  /// <returns>List<Supply></returns>
  Future<List<Supply>> toList([VoidCallback supplyList(List<Supply> o)]) async {
    _buildParameters();
    final suppliesFuture = _obj._mnSupply.toList(qparams);
    final List<Supply> suppliesData = List<Supply>();
    final data = await suppliesFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      suppliesData.add(Supply.fromMap(data[i] as Map<String, dynamic>));
    }
    if (supplyList != null) supplyList(suppliesData);
    return suppliesData;
  }

  /// Returns List<DropdownMenuItem<Supply>>
  Future<List<DropdownMenuItem<Supply>>> toDropDownMenu(
      String displayTextColumn,
      [VoidCallback dropDownMenu(List<DropdownMenuItem<Supply>> o)]) async {
    _buildParameters();
    final suppliesFuture = _obj._mnSupply.toList(qparams);

    final data = await suppliesFuture;
    final int count = data.length;
    final List<DropdownMenuItem<Supply>> items = List();
    items.add(DropdownMenuItem(
      value: Supply(),
      child: Text("Select Supply"),
    ));
    for (int i = 0; i < count; i++) {
      items.add(
        DropdownMenuItem(
          value: Supply.fromMap(data[i] as Map<String, dynamic>),
          child: Text(data[i][displayTextColumn].toString()),
        ),
      );
    }
    if (dropDownMenu != null) {
      dropDownMenu(items);
    }
    return items;
  }

  /// Returns List<DropdownMenuItem<int>>
  Future<List<DropdownMenuItem<int>>> toDropDownMenuInt(
      String displayTextColumn,
      [VoidCallback dropDownMenu(List<DropdownMenuItem<int>> o)]) async {
    _buildParameters();
    qparams.selectColumns = ["id", displayTextColumn];
    final suppliesFuture = _obj._mnSupply.toList(qparams);

    final data = await suppliesFuture;
    final int count = data.length;
    final List<DropdownMenuItem<int>> items = List();
    items.add(DropdownMenuItem(
      value: 0,
      child: Text("Select Supply"),
    ));
    for (int i = 0; i < count; i++) {
      items.add(
        DropdownMenuItem(
          value: data[i]["id"] as int,
          child: Text(data[i][displayTextColumn].toString()),
        ),
      );
    }
    if (dropDownMenu != null) {
      dropDownMenu(items);
    }
    return items;
  }

  /// This method always returns Primary Key List<int>.
  /// <returns>List<int></returns>
  Future<List<int>> toListPrimaryKey(
      [VoidCallback idList(List<int> o), bool buildParameters = true]) async {
    if (buildParameters) _buildParameters();
    final List<int> idData = List<int>();
    qparams.selectColumns = ["id"];
    final idFuture = await _obj._mnSupply.toList(qparams);

    final int count = idFuture.length;
    for (int i = 0; i < count; i++) {
      idData.add(idFuture[i]["id"] as int);
    }
    if (idList != null) {
      idList(idData);
    }
    return idData;
  }

  /// Returns List<dynamic> for selected columns. Use this method for "groupBy" with min,max,avg..
  /// Sample usage: (see EXAMPLE 4.2 at https://github.com/hhtokpinar/sqfEntity#group-by)
  Future<List<dynamic>> toListObject(
      [VoidCallback listObject(List<dynamic> o)]) async {
    _buildParameters();

    final objectFuture = _obj._mnSupply.toList(qparams);

    final List<dynamic> objectsData = List<dynamic>();
    final data = await objectFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      objectsData.add(data[i]);
    }
    if (listObject != null) {
      listObject(objectsData);
    }
    return objectsData;
  }

  /// Returns List<String> for selected first column
  /// Sample usage: await Supply.select(columnsToSelect: ["columnName"]).toListString()
  Future<List<String>> toListString(
      [VoidCallback listString(List<String> o)]) async {
    _buildParameters();

    final objectFuture = _obj._mnSupply.toList(qparams);

    final List<String> objectsData = List<String>();
    final data = await objectFuture;
    final int count = data.length;
    for (int i = 0; i < count; i++) {
      objectsData.add(data[i][qparams.selectColumns[0]].toString());
    }
    if (listString != null) {
      listString(objectsData);
    }
    return objectsData;
  }
}
// endregion SupplyFilterBuilder

// region SupplyFields
class SupplyFields {
  static TableField _fId;
  static TableField get id {
    _fId = SqlSyntax.setField(_fId, "id", DbType.integer);
    return _fId;
  }

  static TableField _fName;
  static TableField get name {
    _fName = SqlSyntax.setField(_fName, "name", DbType.text);
    return _fName;
  }

  static TableField _fDate;
  static TableField get date {
    _fDate = SqlSyntax.setField(_fDate, "date", DbType.integer);
    return _fDate;
  }

  static TableField _fKmOld;
  static TableField get kmOld {
    _fKmOld = SqlSyntax.setField(_fKmOld, "kmOld", DbType.real);
    return _fKmOld;
  }

  static TableField _fKmNow;
  static TableField get kmNow {
    _fKmNow = SqlSyntax.setField(_fKmNow, "kmNow", DbType.real);
    return _fKmNow;
  }

  static TableField _fQtyLiters;
  static TableField get qtyLiters {
    _fQtyLiters = SqlSyntax.setField(_fQtyLiters, "qtyLiters", DbType.real);
    return _fQtyLiters;
  }

  static TableField _fAvg;
  static TableField get avg {
    _fAvg = SqlSyntax.setField(_fAvg, "avg", DbType.real);
    return _fAvg;
  }

  static TableField _fExpense;
  static TableField get expense {
    _fExpense = SqlSyntax.setField(_fExpense, "expense", DbType.real);
    return _fExpense;
  }

  static TableField _fGainGross;
  static TableField get gainGross {
    _fGainGross = SqlSyntax.setField(_fGainGross, "gainGross", DbType.real);
    return _fGainGross;
  }

  static TableField _fGainNet;
  static TableField get gainNet {
    _fGainNet = SqlSyntax.setField(_fGainNet, "gainNet", DbType.real);
    return _fGainNet;
  }

  static TableField _fGainKm;
  static TableField get gainKm {
    _fGainKm = SqlSyntax.setField(_fGainKm, "gainKm", DbType.real);
    return _fGainKm;
  }

  static TableField _fTruckId;
  static TableField get truckId {
    _fTruckId = SqlSyntax.setField(_fTruckId, "truckId", DbType.integer);
    return _fTruckId;
  }

  static TableField _fGasStationId;
  static TableField get gasStationId {
    _fGasStationId =
        SqlSyntax.setField(_fGasStationId, "gasStationId", DbType.integer);
    return _fGasStationId;
  }
}
// endregion SupplyFields

//region SupplyManager
class SupplyManager extends SqfEntityProvider {
  SupplyManager() : super(MyDbModel(), tableName: _tableName, colId: _colId);
  static String _tableName = "supply";
  static String _colId = "id";
}
//endregion SupplyManager
