import 'package:flutter/material.dart';
import 'package:truckbrabo/config/database_config.dart';
import 'package:truckbrabo/splashscreen.dart';
// import 'package:truckbrabo/splashscreen.dart';

String _modelString;
String get modelString => _modelString;

set modelString(String modelStrings) {
  _modelString = modelStrings;
}

void main() async { 
  final String sqfEntityModelString = 'asdf';
  // Atualizar o banco só vim aki e descomentar e comentar o de cima....
  // final String sqfEntityModelString = MyDbModel().createModel();

  final bool isInitialized = await MyDbModel().initializeDB();
  if (isInitialized == true) {
    print("Deu Certooo UhuhuUllll!!!");
    runApp(MyApp(sqfEntityModelString));
  } else {
    print('Deu Merdaaa Banco');
  }
}
class MyApp extends StatelessWidget {
  MyApp(String setmodelString) {
    modelString = setmodelString;
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TruckBrabo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Color(0xFF1DCC8C)
      ),
      home: SplashScreen()
    );
  }
}

class CopiarModeloGerado extends StatelessWidget {
  final TextEditingController txtModel =
      TextEditingController(text: modelString);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SqfEntity Model Creator"),
        leading: Icon(Icons.assignment),
      ),
      body: _buildHomePage(context),
    );
  }

    Container _buildHomePage(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      // hack textfield height
      padding: EdgeInsets.only(bottom: 40.0),
      child: TextField(
        controller: txtModel,
        maxLines: 99,
      ),
    );
  }
}