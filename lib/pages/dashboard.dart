import 'package:truckbrabo/components/box-znt/box-znt-widget.dart';
import 'package:truckbrabo/config/models.dart';
import 'package:flutter/material.dart';
import 'package:truckbrabo/widget/form-supply.dart';

var listaCaminhao;

class DashBoard extends StatefulWidget {
  @override
  DashBoardState createState() {
    cachorrando();
    return DashBoardState();
  }
}

Future<List> cachorrando() async {
  listaCaminhao = await Truck().select().toList();
  listaCaminhao = ['arroz', 'feijao', 'macarrão'];
  return listaCaminhao;
}

class DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: .0,
        leading: Icon(Icons.directions_bus),
        title: Text('TruckBrabo'),
        centerTitle: true,
        actions: <Widget>[
          FlatButton(
            child: Icon(Icons.adjust),
            textColor: Colors.white,
            onPressed: () {
              onCreate();
            },
          )
        ],
      ),
      body: meuLayoutWidget(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) => BoxZnt(
                  title: Text('Registrar Abastecimento'),
                  icon: Icon(Icons.local_gas_station),
                  body: SingleChildScrollView(
                    child: FormSupply(),
                  )));
        },
        foregroundColor: Colors.white,
      ),
    );
  }

  Widget onCreate() {
    return Container(
      child: BoxZnt(
        title: Text('Registrar Abastecimento'),
        icon: Icon(Icons.local_gas_station),
        body: Container(
          child: Text(''),
        ),
      ),
    );
  }

  /// lista de caminhao
  void queryTruck() async {
    final caminhao = await Truck().select().toList();
    for (int i = 0; i < caminhao.length; i++) {
      print(caminhao[i].toMap());
    }
  }

  Widget meuLayoutWidget() {
    print('CAMINHOES');
    print(listaCaminhao);
    // print(listaCaminhao[2].truck);
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF30C1FF),
              Color(0xFF2AA7DC),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: listaCaminhao.length <= 0
            ? Center(
                child: emptyState(
                title: 'Oops!',
                message: 'Ainda Não temos nenhum registro salvo :)',
              ))
            : listaSupply());
  }

  Widget listaSupply() {
    return ListView.builder(
      itemCount: listaCaminhao.length,
      itemBuilder: (_, i) {
        return Card(
          child: ListTile(
              leading: Icon(Icons.directions_bus, size: 30),
              title: Text(listaCaminhao[i]),
              subtitle: Text(listaCaminhao[i]),
              onTap: () => debugPrint(listaCaminhao[i])),
          color: Colors.white,
          elevation: 3.0,
        );
      },
    );
  }

  Widget emptyState({
    title: '',
    message: '',
  }) {
    return Material(
      borderRadius: BorderRadius.circular(16),
      elevation: 16,
      color: Theme.of(context).cardColor.withOpacity(.95),
      shadowColor: Theme.of(context).accentColor.withOpacity(.5),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(title, style: Theme.of(context).textTheme.headline),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(message),
            )
          ],
        ),
      ),
    );
  }
}
