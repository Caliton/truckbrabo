import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

typedef OnDelete();

class FormSupply extends StatefulWidget {
  FormSupply({Key key}) : super(key: key);
  final state = _FormSupplyState();
  @override
  _FormSupplyState createState() => state;
}

class _FormSupplyState extends State<FormSupply> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 25),
      child: Form(
        key: _formKey,
          child: Wrap(
        spacing: 20,
        runSpacing: 20,
        children: <Widget>[
          SizedBox(height: 10,),
          fieldZnt(
              fieldName: 'Posto',
              icon: Icons.store,
              inputType: TextInputType.text,),
          fieldZnt(
              fieldName: 'Km Antigo',
              icon: Icons.slow_motion_video,
              inputType: TextInputType.number),
          fieldZnt(
              fieldName: 'Km Atual',
              icon: Icons.slow_motion_video,
              inputType: TextInputType.number),
          fieldZnt(
              fieldName: 'Média',
              icon: Icons.content_paste,
              inputType: TextInputType.number),
          fieldZnt(
              fieldName: 'Abastecer',
              icon: Icons.local_gas_station,
              inputType: TextInputType.number),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              color: Color(0xFF1DCC8C),
              textColor: Colors.white,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  debugPrint('Caralhada de porra');
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text('Processing Data'))); // manda mensagem
                  Navigator.pop(context);
                } else {debugPrint('Enviaram tudo vazio nesse caraio');}
              },
              child: Text('Salva Caralho'),
            ),
          ),
        ],
      )),
    );
  }

  Widget fieldZnt({String fieldName = 'Campo: ', IconData icon, TextInputType inputType, Function funcao}) {
    return TextFormField(
      onChanged: funcao,
      keyboardType: inputType,
      validator: (value) {
        if (value.isEmpty) {
          return 'Preenche a porra dos Campos!';
        }
        return null;
      },
      textCapitalization: TextCapitalization.words,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 20),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white38,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(30))),
        prefixIcon: Icon(
          icon,
          size: 30,
        ),
        hoverColor: Colors.brown,
        hintText: 'Diga nome do Posto',
        labelText: fieldName,
      ),
      onSaved: (String value) {
        print('PORRADA');
        debugPrint(value);
      },
    );
  }

    Widget dataField({
    String fieldName = "Data",
    IconData icon,
  }) {
    final format = DateFormat("dd-MM-yyyy");
    bool _inputIsValid = true;
    return Center(
      child: Container(
        width: 300,
        height: 70,
        padding: EdgeInsets.all(5.0),
        child: DateTimeField(
          format: format,
          onShowPicker: (context, currentValue) {
            return showDatePicker(
                context: context,
                firstDate: DateTime(1900),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime(2100));
          },
          style: TextStyle(
            fontSize: 22.0,
            height: 1.0,
            color: Colors.black,
            fontFamily: 'Poppins',
          ),
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.date_range),
            labelText: fieldName,
            errorText: _inputIsValid
                ? null
                : 'Por favor preciso saber quem é hahahahha',
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
          ),
        ),
      ),
    );
  }


  Widget moneyField() {
    final _inputIsValid = true;
    return Center(
      child: Container(
        width: 300,
        height: 70,
        padding: EdgeInsets.all(5.0),
        child: TextField(
          inputFormatters: [
            WhitelistingTextInputFormatter.digitsOnly,
            new CurrencyInputFormatter()
          ],
          keyboardType: TextInputType.number,
          textCapitalization: TextCapitalization.words,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 22.0,
            height: 1.0,
            color: Colors.black,
            fontFamily: 'Poppins',
          ),
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.credit_card),
            labelText: "Custo do Combustível",
            errorText: _inputIsValid
                ? null
                : 'Por favor preciso saber quem é hahahahha',
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
          ),
        ),
      ),
    );
  }

}

class CurrencyInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      print(true);
      return newValue;
    }

    double value = double.parse(newValue.text);

    final formatter = new NumberFormat("###,###.###", "pt-br");
    String newText = formatter.format(value);
    if (value != 0) {
      newText = formatter.format(value / 100);
    }
    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}